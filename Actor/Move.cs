﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public GameObject player_meshes;
    public Transform player_head;
    public float speed = 10;
    public float rotation_speed = 1000;
    public float rotation_speed_butt = 150;
    Animation anim_controller;

    public GameObject player;

    public bool can_move = true;

    void Start()
    {
        anim_controller = player_meshes.GetComponent<Animation>();

        UIEvents.current.on_pause_game += change_move_status;
    }

    void rotate_this()
    {
        player.transform.parent = null;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, player_head.transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        player.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;

        if (can_move)
        {
            if (Input.GetAxisRaw("Jump") == 1) anim_controller.Play("Attack");

            else if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                float hor_dir = Input.GetAxisRaw("Horizontal");
                float ver_dir = Input.GetAxisRaw("Vertical");
                Player.player.current_state = Player.State.RUNNING; //anim_controller.Play("Run");

                rotate_this();
                //player_meshes.transform.rotation = Quaternion.Euler(player_head.transform.rotation.eulerAngles);

                float ver_rotation = ver_dir;
                float hor_rotation = hor_dir;
                if (ver_dir == 1)
                {
                    ver_rotation = 0;
                    hor_rotation = hor_rotation / 2;
                }
                else if (ver_dir == -1)
                {
                    hor_rotation = -hor_rotation / 2;
                }
                float y_rotation = player_head.transform.rotation.eulerAngles.y + 90 * hor_rotation + ver_rotation * 180;
                Quaternion rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, y_rotation, transform.rotation.eulerAngles.z);
                player_meshes.transform.rotation = Quaternion.RotateTowards(player_meshes.transform.rotation, rotation, rotation_speed * dt);

                Vector3 move = new Vector3(hor_dir, 0f, ver_dir) * speed * dt;
                transform.Translate(move, Space.Self);
            }

            else if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0) Player.player.current_state = Player.State.IDLE; //anim_controller.Play("idle");

            if (Input.GetAxisRaw("RotateY") != 0)
            {
                float axis = Input.GetAxisRaw("RotateY");
                transform.Rotate(Vector3.up, axis * rotation_speed_butt * dt);
            }
        }
    }

    void change_move_status()
    {
        can_move = !(can_move);
    }

    private void OnDestroy()
    {
        UIEvents.current.on_pause_game -= change_move_status;
    }
}
