﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public State current_state;
    public static Player player;

    public GameObject player_meshes;
    Animation anim_controller;

    public enum State
    {
        ATTACKING,
        IDLE,
        WALKING,
        RUNNING
    }

    private void Awake()
    {
        current_state = State.IDLE;
        player = this;
    }

    private void Start()
    {
        anim_controller = player_meshes.GetComponent<Animation>();
    }

    private void Update()
    {
        if (current_state == State.IDLE) anim_controller.Play("idle");
        if (current_state == State.RUNNING) anim_controller.Play("Run");
    }
}
