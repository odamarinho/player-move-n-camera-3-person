﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public int scroll_speed;
    public float cam_max_speed;
    public int player_max_dis;
    public float player_min_dis;
    public Transform player_head;
    public Transform cam_rotator;

    public bool can_move_camera = true;

    float player_dis;

    float cam_speed;

    float move_cam_x = 0;
    float move_cam_y = 0;

    float prev_dist;

    bool cam_hitted = false;

    float axis_inverter = 1;

    // Start is called before the first frame update
    void Start()
    {
        player_dis = Vector3.Distance(player_head.position, transform.position);
        cam_speed = cam_max_speed / 2;

        UIEvents.current.on_pause_game += change_movecamera_status;
        UIEvents.current.on_change_sen += change_cam_speed;
        UIEvents.current.on_invert_y_axis += invert_y_axis;
    }

    void FixedUpdate()
    {
        RaycastHit cam_hit;
        //RaycastHit dist_hit;
        if (Physics.Raycast(player_head.transform.position, -transform.forward, out cam_hit, player_dis))
        {
            if(!cam_hitted) prev_dist = player_dis; 
            transform.position = cam_hit.point;
            Debug.DrawRay(player_head.transform.position, -transform.forward * cam_hit.distance, Color.yellow);
            Debug.Log(cam_hit.transform.gameObject.name);

            cam_hitted = true;
        }

        else if(cam_hitted)
        {
            float atual_dis = Vector3.Distance(player_head.position, transform.position);
            transform.Translate(0, 0, -(player_dis - atual_dis));
            cam_hitted = false;
        }
    }

    void LateUpdate()
    {
        float dt = Time.deltaTime;

        if(can_move_camera)
        {
            if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
            {
                if (player_dis <= player_max_dis && player_dis >= player_min_dis)
                {
                    float scroll = Input.GetAxisRaw("Mouse ScrollWheel") * 10;
                    player_dis -= scroll_speed * dt * scroll;

                    if (player_dis < player_min_dis) player_dis = player_min_dis;
                    else if (player_dis > player_max_dis) player_dis = player_max_dis;
                    else
                    {
                        transform.Translate(0, 0, scroll * scroll_speed * dt, Space.Self);
                    }

                    transform.LookAt(player_head);
                }
            }

            if ((Input.GetAxisRaw("Mouse X") != 0 || Input.GetAxisRaw("Mouse Y") != 0) && (Input.GetAxisRaw("Fire1") == 1))
            {
                move_cam_x += Input.GetAxis("Mouse X") * cam_speed * dt;
                move_cam_y -= Input.GetAxis("Mouse Y") * cam_speed * dt * axis_inverter;

                move_cam_y = Mathf.Clamp(move_cam_y, -45, 50);
                cam_rotator.rotation = Quaternion.Euler(move_cam_y, move_cam_x, 0);

                player_head.rotation = Quaternion.Euler(0, move_cam_x, 0);

                transform.LookAt(player_head);
            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                cam_rotator.rotation = new Quaternion();
            }
        }
    }

    public void change_cam_speed(float value)
    {
        cam_speed = cam_max_speed * value;
    }

    public float get_cam_player_distance()
    {
        return Vector3.Distance(player_head.position, transform.position);
    }

    void change_movecamera_status()
    {
        can_move_camera = !(can_move_camera);
    }

    void invert_y_axis(float i)
    {
        axis_inverter = i;
    }

    private void OnDestroy()
    {
        UIEvents.current.on_pause_game -= change_movecamera_status;
        UIEvents.current.on_change_sen -= change_cam_speed;
        UIEvents.current.on_invert_y_axis -= invert_y_axis;
    }
}
