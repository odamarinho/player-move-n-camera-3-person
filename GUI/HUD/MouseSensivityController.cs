﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseSensivityController : MonoBehaviour
{
    public PlayerCamera main_camera;
    public Scrollbar ms_sb;

    public void change_camera_sensivity()
    {
        main_camera.change_cam_speed(ms_sb.value);
    }
}
