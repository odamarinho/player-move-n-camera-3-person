﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject menu_pause_panel;
    public Scrollbar sen_scroll_bar;
    public Toggle y_axis_inverter;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIEvents.current.pause_game();
            menu_pause_panel.SetActive(!menu_pause_panel.activeSelf);
        }
    }

    public void change_sen()
    {
        float value = sen_scroll_bar.GetComponent<Scrollbar>().value;
        UIEvents.current.change_sen(value);
    }

    public void invert_y_axis()
    {
        float i;
        i = y_axis_inverter.isOn ? -1 : 1;
        UIEvents.current.invert_y_axis(i);
    }
}
