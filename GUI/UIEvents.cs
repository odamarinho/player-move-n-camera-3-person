﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEvents : MonoBehaviour
{
    public static UIEvents current;
    public event Action on_pause_game;
    public event Action<float> on_change_sen;
    public event Action<float> on_invert_y_axis;

    void Awake()
    {
        current = this;
    }

    public void pause_game()
    {
        on_pause_game.Invoke();
    }

    public void change_sen(float value)
    {
        on_change_sen.Invoke(value);
    }

    public void invert_y_axis(float i)
    {
        on_invert_y_axis(i);
    }

}
